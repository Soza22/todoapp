(function() {
	'use strict';

	angular
	.module('angularSeedApp')
	.controller('MainController', MainController);

	/** @ngInject */
	function MainController(MainService) {
		var vm = this;
		vm.tasks = MainService.getTasks();
		vm.filter={};
    vm.addTask=addTask;
    vm.filterTasks=filterTasks;

		function addTask(key){
			if(key===13){
				vm.tasks.push({"name":vm.newTask,"status":"Active"});
			}

		}
    
		function filterTasks(status){
			vm.filter.status=status;
		}
	}

})();
